{ flake, ... }:
{
  boot.loader.systemd-boot.enable = true;
  fileSystems."/".device = "/dev/sda1";
  nix.registry.sys.flake = flake.inputs.nixpkgs;
}
