{
  description = "A very basic flake";
  outputs = { self, nixpkgs }: {
    nixosConfigurations.foo = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [ ./module.nix ];
      extraArgs = { flake = self; };
    };
  };
}
